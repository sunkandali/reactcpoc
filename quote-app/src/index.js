import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class App extends React.Component {
    state = {
        showFormSuccess: false,
        cars : [
            {
                name: '2018 GENESIS G80',
                variants :
                [
                    {name: '3.8L V6 8-Speed Automatic with SHIFTRONIC® RWD', type: ['Standard for 3.8','3.8 Premium package','3.8 Ultimate package']},
                    {name: '3.8L V6 8-Speed Automatic with SHIFTRONIC® AWD', type: ['Standard for 3.8','3.8 Premium package','3.8 Ultimate package']},
                    {name: '5.0L V6 8-Speed Automatic with SHIFTRONIC® RWD', type: ['5.0 Ultimate package']},
                    {name: '5.0L V6 8-Speed Automatic with SHIFTRONIC® AWD', type: ['5.0 Ultimate package']}
                ],
                img : 'https://www.genesis.com/content/dam/genesis/us/com/global/2018-g80-50-ult-himalayan-gray.png'
            },
            {
                name:'2018 GENESIS G80 Sport',
                variants :
                [
                    {name: '3.3T V6 8-Speed Automatic with SHIFTRONIC® RWD', type: ['3.3T SPORT']},
                    {name: '3.3T V6 8-Speed Automatic with SHIFTRONIC® AWD', type: ['3.3T SPORT']}
                ],
                img : 'https://www.genesis.com/content/dam/genesis/us/com/global/2018-g80-sport-sevilla-red.png'
            },
            {
                name:'2018 GENESIS G90',
                variants :
                [
                    {name: '3.3T 8-Speed Automatic Transmission RWD', type: ['3.3T PREMIUM']},
                    {name:'3.3T 8-Speed Automatic Transmission AWD', type: ['3.3T PREMIUM']},
                    {name:'5.0 8-Speed Automatic Transmission RWD', type: ['5.0 ULTIMATE']},
                    {name:'5.0 8-Speed Automatic Transmission AWD', type: ['5.0 ULTIMATE']}
                ],
                img : 'https://www.genesis.com/content/dam/genesis/us/com/global/2017-g90-33-caspian-black.png'
            }
        ],
        selectedCar: '2018 GENESIS G80',
        selectedTransmission: '3.8L V6 8-Speed Automatic with SHIFTRONIC® RWD',
        errorFields: ["First Name", "Last Name", "Email", "Phone", "Zip Code"]
    };

    validate = (e) => {
        var input_list = document.getElementsByClassName("form-control");
        var tempList = [];
        for(var count= 0; count< input_list.length-1; count++){
            if(input_list[count].value.trim().length == 0 ){
                tempList.push(input_list[count].previousElementSibling.innerHTML);
            }
        }
        this.setState({errorFields: tempList});
        console.log(this.state.errorFields);
    }  

    toggleCheck = () => {
        var x = document.getElementsByClassName("checkSign")[0].children[0].children[0].hidden;
        if(x)
            document.getElementsByClassName("checkSign")[0].children[0].children[0].hidden = false;
        else
            document.getElementsByClassName("checkSign")[0].children[0].children[0].hidden = true;    
    }

    selectCar = (e) =>{
        this.setState({selectedCar: e.target.value});
        console.log(this.state.selectedCar);

        var trans = this.state.cars.filter(car => {
            return car.name === e.target.value
        })
        document.getElementById("trim_powertrain_ul").selectedIndex = 0;
        this.setState({selectedTransmission: trans[0].variants[0]});
    }

    selectTransmission = (e) =>{
        this.setState({selectedTransmission: e.target.value});
        console.log(this.state.selectedTransmission);
    }

    /**
     * Renders the page UI. Uses the Form component defined above
     * instead of the usual html form.
     * Uses Bootstrap layout classes.
     **/
    render() {
        const carImageSrc = this.state.carImageSrc; 
        
        var quote = {
            quoteTitle: 'REQUEST A QUOTE',
            quoteSubTitle: 'GET REAL OFFERS ON A NEW GENESIS',
            step1Title : '1. CONFIGURE YOUR GENESIS',
            step1SubTitle : 'Build your own Genesis.',
            step2Title : '2. ENTER CONTACT INFO',
            step2SubTitle : 'Please let us know how we may reach you to help find your Genesis.',
            disclaimer : 'By checking the box, you consent to Hyundai Motor America using automatic telephone dialing systems to call and/or send you periodic SMS marketing messages to the number you have provided. You do not have to consent in order to obtain any of our products or services. Message and data rates may apply.',
            requestButtonText :'Request a Quote',
        };

        var car =  this.state.cars.filter(car => {
            return car.name === this.state.selectedCar
        })
        
        var transmission = car[0].variants[0];
        var temp = car[0].variants.filter(trans => {
            return trans.name === this.state.selectedTransmission
        })
        if(temp.length > 0){
            transmission = temp[0];
        }
        
        return (
            <div id="app" className="enclosing-tag"> 
            <img className="logo-header" src="https://www.genesis.com/content/dam/genesis/worldwide/images/common/logo_header.png" />
            <div className="container">
                <div className="column-layout" >
                <div id="content" className="site_container" >
                <div className="container-fluid" >
                    <div className="row" >
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-col-xs12" >
                            <div className="content" >
                                <div className="genesispremium-title-text" >
                                    <div id="titletext" className="row pageTitleWrapper" >
                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center " >
                                            <h1 className="title">{quote.quoteTitle}</h1>
                                            <h6 className="subtitle" >{quote.quoteSubTitle}</h6>
                                            <hr className="notop-margin"/>
                                        </div>
                                    </div>

                                </div>

                                <form id="raqstdform" method="post" action="javascript:void(0)"  className="requiredCl">
                                    <div className="row" >
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                                            <h3 className="headline" >{quote.step1Title}</h3>
                                            <p  className="requiredCl">{quote.step1SubTitle}</p>
                                            <div className="clearfix" ></div>
                                            <div className="form-group form-sel-opt" >
                                                <div id="vehicle" className="dropdown" >
                                                    <div className="selectSign"></div>
                                                    <select className="required" id="vehicle_ul" onChange={this.selectCar} value = {this.state.selectedCar}>
                                                    {
                                                        this.state.cars.map((car, i) => {
                                                            return <option>{car.name}</option>
                                                        })
                                                    }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="clearfix" ></div>
                                            <div className="form-group form-sel-opt" >
                                                <div id="trim_powertrain" className="dropdown " >
                                                    <select id="trim_powertrain_ul"  className="requiredCl" onChange={this.selectTransmission}>
                                                    {
                                                        car[0].variants.map((variant, i) => {
                                                            return <option>{variant.name}</option>
                                                        })
                                                    }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="clearfix" ></div>
                                            <div className="form-group form-sel-opt" >
                                                <div id="package" className="dropdown " >
                                                    <select id="package_ul"  className="requiredCl">
                                                    {   
                                                        transmission.type.map((t, i) => {
                                                            return <option>{t}</option>
                                                        })
                                                    }
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="clearfix" ></div>

                                        </div>
                                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 configure text-center" >
                                            <img id="carimage" src={car[0].img} alt=""  className="requiredCl" />
                                            <p id="carmodel" className="text-center text-uppercase" >2018 GENESIS G80</p>
                                        </div>
                                    </div>

                                    <div className="row" >
                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 contactinfo" >
                                            <h3 className="headline" >{quote.step2Title}</h3>
                                            <p  className="requiredCl">{quote.step2SubTitle}</p>
                                            <div className="clearfix" ></div>
                                            <div className="row" >
                                                <div className="col-lg-6 col-md-6  col-sm-6 col-xs-12" >
                                                    <div className="form-group" >
                                                        <label htmlFor="firstname"  className="requiredCl">First Name</label>
                                                        <input type="text" className="form-control" id="firstname" placeholder="First Name" tabIndex="1" maxLength="25"  />
                                                    </div>
                                                    <div className="clearfix" ></div>
                                                    <div className="form-group" >
                                                        <label htmlFor="lasttname"  className="requiredCl">Last Name</label>
                                                        <input type="text" className="form-control" id="lastname" placeholder="Last Name" tabIndex="2" maxLength="25"  />
                                                    </div>
                                                    <div className="clearfix" ></div>
                                                    <div className="form-group" >
                                                        <label htmlFor="email"  className="requiredCl">Email</label>
                                                        <input type="email" className="form-control" id="email" placeholder="you@mail.com" tabIndex="3" maxLength="50"  />
                                                    </div>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12" >

                                                    <div className="clearfix" ></div>
                                                    <div className="form-group" >
                                                        <label htmlFor="phone"  className="requiredCl">Phone</label>
                                                        <input type="tel" className="form-control" id="phone" placeholder="(555) 555-5555" tabIndex="4" maxLength="10"  />
                                                    </div>
                                                    <div className="clearfix" ></div>
                                                    <div className="form-group" >
                                                        <label htmlFor="zip"  className="requiredCl">Zip Code</label>
                                                        <input type="tel" className="form-control" id="zip" tabIndex="5" placeholder="90000" maxLength="5"  />
                                                    </div>
                                                    <div className="clearfix" ></div>

                                                    <div className="form-group nomandate" >
                                                        <label htmlFor="comments"  className="requiredCl">Comments</label>
                                                        <input type="text" className="form-control" id="comments" tabIndex="5" maxLength="200"  />

                                                    </div>
                                                </div>

                                                <div className="col-lg-12 col-xs-12" >
                                                    <span className="required" >REQUIRED</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row" >
                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                            <div className="form-group" >
                                                <div className="terms  mctatop" >
                                                    <input type="checkbox" id="consentchk" name="models" value="g90"  className="requiredCl" />
                                                    <label htmlFor="consentchk"  className="checkSign" onClick={this.toggleCheck}> <i  className="requiredCl"><img src="https://www.genesis.com/content/dam/genesis/us/images/raq/ico_chk_model.png" alt=""  className="requiredCl" /></i></label>
                                                    <section  className="requiredCl">{quote.disclaimer}</section>
                                                </div>
                                            </div>
                                            <div className="row" >
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" >
                                                    <button id="raqsubmit" tabIndex="8" className="primary" data-toggle="modal" data-target="#error-modal" onClick={this.validate}>{quote.requestButtonText}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="modal fade" id="error-modal" tabIndex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                        <div className="modal-dialog modal-md">
                                            <div tabIndex="-1" role="document" aria-labelledby="modal1___BV_modal_header_" aria-describedby="modal1___BV_modal_body_" className="modal-content">
                                                <header id="modal1___BV_modal_header_" className="modal-header">
                                                    <h5 className="modal-title">Following fields are mandatory</h5>
                                                    <button type="button" aria-label="Close" className="close" data-dismiss="modal">×</button>
                                                </header>
                                                <div id="modal1___BV_modal_body_" className="modal-body">
                                                    <div className="d-block">
                                                        <div className="list-group">
                                                        {
                                                            this.state.errorFields.map((field, i) => {
                                                                return <div className="list-group-item">{field}</div>
                                                            })
                                                        }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-1 col-md-1 hidden-sm hidden-xs" ></div>
                </div>
            </div>
        </div></div>
</div>

        );
    }
}

//Render the App component inside the main div with id equal to "app"
ReactDOM.render(<App />, document.getElementById("root"));